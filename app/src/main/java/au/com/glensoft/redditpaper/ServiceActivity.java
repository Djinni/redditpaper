package au.com.glensoft.redditpaper;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

public class ServiceActivity extends FragmentActivity {
    
   private Alarm alarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = this.getSharedPreferences("au.com.glensoft.redditpaper", Context.MODE_PRIVATE);
        setContentView(R.layout.service_manager);
        
        ToggleButton serviceToggle = this.findViewById(R.id.serviceToggle);
        final EditText targetSubreddit = this.findViewById(R.id.subredditText);
        targetSubreddit.setText(prefs.getString("subreddit", "mtgporn"));
        final Spinner interval = this.findViewById(R.id.spinner);
        interval.setSelection(prefs.getInt("interval", 0));

        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager.getNextAlarmClock() != null) {
            serviceToggle.setChecked(true);
        } else {
            serviceToggle.setChecked(false);
        }
        serviceToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()  {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
                    Integer[] values = {1, 2, 3, 4, 8, 24};
                    int intervalNum = values[interval.getSelectedItemPosition()];
                    SharedPreferences prefs = getSharedPreferences("au.com.glensoft.redditpaper", Context.MODE_PRIVATE);
                    prefs.edit().putString("subreddit", targetSubreddit.getText().toString()).apply();
                    prefs.edit().putInt("interval", intervalNum).apply();
					ServiceActivity.this.alarm = new Alarm();
					alarm.SetAlarm(ServiceActivity.this, targetSubreddit.getText().toString(), intervalNum);
				} else {
					alarm.CancelAlarm(ServiceActivity.this);
				}
			}
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
      super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putBoolean("servicetoggle", ((ToggleButton)this.findViewById(R.id.serviceToggle)).isChecked());
        savedInstanceState.putInt("interval", ((Spinner)this.findViewById(R.id.spinner)).getSelectedItemPosition() );
        savedInstanceState.putString("subreddit", ((EditText)this.findViewById(R.id.subredditText)).getText().toString() );
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
      super.onRestoreInstanceState(savedInstanceState);
        ((ToggleButton)this.findViewById(R.id.serviceToggle)).setChecked(savedInstanceState.getBoolean("servicetoggle"));
        ((Spinner)this.findViewById(R.id.spinner)).setSelection(savedInstanceState.getInt("interval"));
        ((EditText)this.findViewById(R.id.subredditText)).setText(savedInstanceState.getString("subreddit"));
    }

}
