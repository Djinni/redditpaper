package au.com.glensoft.redditpaper;

import android.app.Service;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WallpaperService extends Service
{
	final String key = "au.com.glensoft.redditpaper.urllist";
    public void onCreate()
    {
        super.onCreate();   
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	String subreddit = this.getSharedPreferences("au.com.glensoft.redditpaper", Context.MODE_PRIVATE).getString("subreddit", "");
    	final String url = "https://www.reddit.com/r/" + subreddit + ".json?limit=40";
        new Thread(new Runnable() {

			@Override
			public void run() {
				List<String> previousImages = createJsonArray();
		        
		    	String imageAddress = null;
				try {
					imageAddress = getImageAddress(url, previousImages);
				} catch (IOException e1) {
					e1.printStackTrace();
					return;
				}
		    	if (imageAddress != null) {
			        saveAddress(imageAddress);
			        
			        Bitmap image  = downloadImage(imageAddress);
			        
			        applyWallpaper(image);
		    	}
		    	WallpaperService.this.stopSelf();
			}
        	
        }).start();
        
    	return START_STICKY;
    }

    private List<String> createJsonArray() {
    	String jsonString = WallpaperService.this.getSharedPreferences("au.com.glensoft.redditpaper", Context.MODE_PRIVATE).getString(key, "{}");
        JSONArray jsonArray;
        List<String> previousImages = new ArrayList<String>();
        try {
        	jsonArray = new JSONArray(jsonString);
	        previousImages = new ArrayList<String>();
	        for (int i=0; i<jsonArray.length(); i++) {
				previousImages.add( jsonArray.getString(i) );
	        }
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
        return previousImages;
    }
    
    private String getImageAddress(String url, List<String> previousImages) throws IOException {
    	String imageAddress = null;
		try {

			URL subredditUrl = new URL(url);

			HttpURLConnection conn = (HttpURLConnection) subredditUrl.openConnection();
			conn.setRequestMethod("GET");
			String jsonString = inputStreamToString(conn.getInputStream());
			JSONObject parent = new JSONObject(jsonString);
			JSONObject data = parent.getJSONObject("data");
			JSONArray children = data.getJSONArray("children");
			int i = 0;
			while (imageAddress == null) {
				JSONObject child = children.getJSONObject(i);
				JSONObject childData = child.getJSONObject("data");
				String URL = childData.getString("url");
				if (URL.contains("?")) {
					URL = URL.split("\\?")[0];
				}
				if (URL.endsWith("png") || URL.endsWith("jpg") || URL.endsWith("jpeg")) {
					if (!previousImages.contains(URL)) {
						imageAddress = URL;
					}
				}
				i++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return imageAddress;
    }

	public String inputStreamToString (InputStream is) {

		String result = "";
		try {

			BufferedReader buffer = new BufferedReader(
					new InputStreamReader(is), 8192);
			String s;
			while ((s = buffer.readLine()) != null) {
				result += s;
			}

			is.close();

		}
		catch (Exception e) {
			e.printStackTrace();
			result = "";
		}
		return result;
	}


	private Bitmap downloadImage(String imageAddress) {
    	Bitmap image = grabImage(imageAddress);
		return image;
    }
    
    private void applyWallpaper(Bitmap image) {
		if (image != null) {
	    	WallpaperManager wallpaperManager = WallpaperManager.getInstance(WallpaperService.this);
	    	try {
				wallpaperManager.setBitmap(image);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    }

    public void onStart(Context context,Intent intent, int startId)
    {

    }
    
    private void saveAddress(String imageAddress) {
    	
    	SharedPreferences prefs = this.getSharedPreferences("au.com.glensoft.redditpaper", Context.MODE_PRIVATE);
    	String list = prefs.getString(key, "");
    	JSONArray jsonList = new JSONArray();
		try {
			jsonList = new JSONArray(list);
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	jsonList.put(imageAddress);
    	prefs.edit().putString(key, jsonList.toString()).apply();
    }
    
    private Bitmap grabImage(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Bitmap d = BitmapFactory.decodeStream(is);
            is.close();
            return d;
        } catch (Exception e) {
        	System.out.println(e);
            return null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) 
    {
        return null;
    }
}